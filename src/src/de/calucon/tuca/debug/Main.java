package de.calucon.tuca.debug;

import de.calucon.tuca.Compiler;
import de.calucon.tuca.CompilerException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Simon on 06.12.2016.
 */
public class Main {

    public static void main(String[] args) throws IOException, CompilerException {

        File f = new File("D:\\", "rawCode.txt");
        f.delete();
        f.createNewFile();

        BufferedWriter bw = new BufferedWriter(new FileWriter(f));

//        bw.write("ERROR this is a test");
//        bw.newLine();
        bw.write("VAR BOOL Simon = true");
        bw.newLine();
        bw.write("VAR BOOL Simon = true");

        bw.flush();
        bw.close();


        Compiler c = new Compiler(f);

    }

}
