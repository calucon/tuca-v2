package de.calucon.tuca.util;

import de.calucon.tuca.Instruction;

/**
 * Created by Simon on 08.12.2016.
 */
public class NegativeComparison {

    public static Instruction negotiate(Instruction instruction){
        switch (instruction){
            case EQU:
                return Instruction.NEQ;
            case NEQ:
                return Instruction.EQU;
            case LSS:
                return Instruction.GEQ;
            case LEQ:
                return Instruction.GTR;
            case GTR:
                return Instruction.LEQ;
            case GEQ:
                return Instruction.LSS;
        }
        return Instruction.NULL;
    }

}
