package de.calucon.tuca.util;

import de.calucon.tuca.Instruction;

/**
 * Created by Simon on 08.12.2016.
 */
public class DatatypeFinder {

    private static final char[] NUMBER = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    private static final char POINT = '.';
    private static final String TRUE = "true";
    private static final String FALSE = "false";

    public static Instruction toDataType(String str){
        int dots_found = 0;
        boolean has_letters = false;
        char[] chars = str.toCharArray();

        if(str.startsWith("\"") && str.endsWith("\""))
            return Instruction.STRING;
        if(str.equalsIgnoreCase(TRUE) || str.equalsIgnoreCase(FALSE))
            return Instruction.BOOLEAN;

        for(char c : chars){
            if(!isNumber(c)){
                if(c == POINT){
                    dots_found++;
                } else {
                    has_letters = true;
                    break;
                }
            }
        }

        if(!has_letters && dots_found <= 1){
            if(dots_found == 1){
                try {
                    Float.parseFloat(str);
                    return Instruction.FLOAT;
                } catch (Exception e){
                    return Instruction.DOUBLE;
                }
            } else {
                try {
                    Integer.parseInt(str);
                    return Instruction.INT;
                } catch (Exception e){
                    return Instruction.LONG;
                }
            }
        } else
            return Instruction.STRING;
    }

//-----------------------------------------------

    private static boolean isNumber(char c){
        for(char ch : NUMBER){
            if(ch == c) return true;
        }
        return false;
    }

}
