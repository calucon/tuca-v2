package de.calucon.tuca;

import java.util.ArrayList;

/**
 * Created by Simon on 02.12.2016.
 */
public class CodeCheck {

    private String[] lines;
    private ArrayList<CompilerException> exceptions = new ArrayList<>();

    public CodeCheck(String[] lines){
        this.lines = lines;

        check();
    }

//-----------------------------------------------

    private void check(){
        exceptions.clear();
        int IF = 0;
        int ELSE = 0;
        int WHILE = 0;

        for(int i = 0; i < lines.length; i++){
            String line = lines[i].trim();

            if(line.length() == 0)
                continue;
            else {

            }
        }

        if(IF != 0) exceptions.add(new CompilerException(-1, "Please take a look at your IF/ELSE Conditions. " + (IF > 0 ? "To many IF" : "non-closed IF block(s)")));
        if(ELSE != 0) exceptions.add(new CompilerException(-1, "Please take a look at your IF/ELSE Conditions. " + (ELSE > 0 ? "To many ELSE" : "non-closed ELSE block(s)")));
        if(WHILE != 0) exceptions.add(new CompilerException(-1, "Please take a look at your IF/ELSE Conditions. " + (WHILE > 0 ? "To many ELSE" : "non-closed WHILE block(s)")));
    }

//-----------------------------------------------

    public ArrayList<CompilerException> getExceptions(){
        return exceptions;
    }

}