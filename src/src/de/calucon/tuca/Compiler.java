package de.calucon.tuca;

import de.calucon.tuca.util.DatatypeFinder;
import de.calucon.tuca.util.NegativeComparison;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by Simon on 05.12.2016.
 */
public class Compiler {

    private  final String FILE_ENDING = ".tuca";
    private ByteArrayOutputStream stream;
    private int lineCount;
    private Map<String, Instruction> variable; //var_name - datatype
    private List<Instruction> lastBrackets;


    public Compiler(File file) throws  IOException, CompilerException{
        Scanner scanner = new Scanner(new FileInputStream(file));
        FileOutputStream writer = new FileOutputStream(getCompiledFile(file));

        compile(scanner);
        writer.write(stream.toByteArray());

        scanner.close();
        writer.flush();
        writer.close();
    }

//-----------------------------------------------

    private File getCompiledFile(File file) throws IOException {
        File compiled = new File(file.getParent(), file.getName().substring(0, file.getName().lastIndexOf('.')) + FILE_ENDING);
        compiled.createNewFile();
        return compiled;
    }

//-----------------------------------------------

    private void compile(Scanner scanner) throws CompilerException, IOException {
        stream = new ByteArrayOutputStream();
        variable = new HashMap<>();
        lastBrackets = new ArrayList<>();
        lineCount = 0;

        while(scanner.hasNext()){
            lineCount++;
            String[] line = scanner.nextLine().trim().replace("\t", "").split(Pattern.quote(" "));

            if(line.length == 0)
                continue;
            else {
                Instruction i0 = Instruction.getByString(line[0]);
                writeStm(i0);

                if(i0.getType() == Instruction.IT.Statement || i0.getType() == Instruction.IT.Logic){
                    line = Arrays.copyOfRange(line, 1, line.length);

                    switch (i0){
                        case ERROR:
                            handleERROR(line);
                            break;
                        case DEFINE_VAR:
                            handleDEF_VAR(line);
                            break;
                        case GOTO:
                            handleGOTO(line);
                            break;
                        case IF:
                            handleIF(line);
                            break;
                        case ELSE:
                            //handleELSE();
                            CompilerException.throwInvalidSyntax(lineCount, "An ELSE must be in the same line as the closing brackets of the IF!");
                            break;
                        case WHILE:
                            handleWHILE(line);
                            break;
                        case MOVE:
                            handleMOVE(line);
                            break;
                        case TURN:
                            handleTURN(line);
                            break;
                        case MINE:
                            handleMINE(line);
                            break;
                        case EXCAVATE:
                            break;
                        case MESSAGE_OWNER:
                            handleMSG_OWNER(line);
                            break;
                    }
                    writeEndl();
                } else {
                    if(i0.getType() == Instruction.IT.BracketC){
                        Instruction i1 = lastBrackets.get(lastBrackets.size()-1);
                        writeStm(i1);

                        writeEndl();
                        if(line.length > 0){ //Only do this if it ends with an if
                            if(i1 == Instruction.IF_END){
                                if(Instruction.getByString(line[1]).equals(Instruction.ELSE)){
                                    writeEndl();
                                    handleELSE();
                                }
                            } else
                                CompilerException.throwInvalidSyntax(lineCount, "An ELSE can't be placed after " + (i1 == Instruction.ELSE_END ? "an ELSE" : "a WHILE"));
                        }
                    } else if(line[0].startsWith(":")){ //goto :
                        handleDEF_GOTO(line);
                        writeEndl();
                    } else if(variable.containsKey(line[0])){
                        handleVAR(Arrays.copyOfRange(line, 1, line.length), line[0]);
                        writeEndl();
                    } else
                        CompilerException.throwNotAStatement(lineCount, i0);
                }
            }
        }
    }

//-----------------------------------------------

    private void handleERROR(String[] line) throws CompilerException {
        if(line.length > 0){
            write(line, 0);
        } else {
            CompilerException.throwNotEnoughArguments(lineCount, Instruction.ERROR, 1);
        }
    }

    private void handleVAR(String[] line, String var_name) throws CompilerException, IOException {
        if(variable.containsKey(var_name)){
            Instruction datatype = variable.get(var_name);
            writeStm(Instruction.LOAD_VAR_TYPE);
            writeStm(datatype);
            writeEndl();

            if(line.length > 1){
                Instruction asg = Instruction.getByString(line[0]);
                if(asg == Instruction.ASG || asg.getType() == Instruction.IT.Math){
                    writeStm(Instruction.LOAD_VAR);
                    write(var_name);
                    writeEndl();

                    writeStm(Instruction.VAR_ASG);
                    writeStm(asg); //define what should happen with the variable -> override, add, sub, mul, div
                    writeDataType(datatype, line);
                } else
                    CompilerException.throwInvalidSyntax(lineCount, line[0] + " = value");
            } else
                CompilerException.throwInvalidSyntax(lineCount, line[0] + " = value");
        } else
            throw new CompilerException(lineCount, "You must define " + var_name + " before you can use it!");
    }

    private void handleDEF_VAR(String[] line) throws CompilerException, IOException {
        if(line.length > 1){
            Instruction datatype = Instruction.getByString(line[0]);
            if(datatype.getType() == Instruction.IT.DataType){
                writeStm(datatype);
                if(!variable.containsKey(line[1])){
                    write(line[1]);
                    variable.put(line[1], datatype);

                    if(line.length > 3){
                        if(Instruction.getByString(line[2]) == Instruction.ASG){
                            writeEndl(); //defining finished, next is assigning it
                            handleVAR(Arrays.copyOfRange(line, 2, line.length), line[1]);
                        } else
                            CompilerException.throwInvalidSyntax(lineCount, "<variable> = <value>");
                    }
                } else
                    throw new CompilerException(lineCount, "The variable " + line[1] + " was already declared!");
            } else
                CompilerException.throwInvalidSyntax(lineCount, "Valid Datatypes: STRING, INT, FLOAT");
        } else
            CompilerException.throwInvalidSyntax(lineCount, "VAR <type> <name>");
    }

    private void handleGOTO(String[] line) throws CompilerException {
        if(line.length == 1){
            write(line[0]);
        } else {
            CompilerException.throwInvalidArgumentCount(lineCount, Instruction.GOTO, 1);
        }
    }

    private void handleDEF_GOTO(String[] line) throws CompilerException {
        if(line.length == 1){
            writeStm(Instruction.GOTO);
            write(line[0].substring(1));
        } else
            CompilerException.throwInvalidArgumentCount(lineCount, Instruction.DEFINE_GOTO, 1);
    }

    private void handleIF(String[] line) throws CompilerException, IOException {
        handleWhileIF(line, false);
    }

    private void handleELSE() {
        writeStm(Instruction.ELSE_START);
        writeEndl();
        lastBrackets.add(Instruction.ELSE_END);
    }

    private void handleWHILE(String[] line) throws IOException, CompilerException {
        handleWhileIF(line, true);
    }

    private void handleMOVE(String[] line) throws CompilerException, IOException {
        if(line.length > 0){
            Instruction datatype = DatatypeFinder.toDataType(line[0]);
            if(datatype == Instruction.INT || datatype == Instruction.LONG){
                writeStm(datatype);
                if(datatype == Instruction.INT) writeInt(Integer.parseInt(line[0]));
                else writeLong(Long.parseLong(line[0]));

                if(line.length == 2){
                    Instruction direction = Instruction.getByString(line[1]);
                    if(direction.getType() == Instruction.IT.Direction){
                        writeStm(direction);
                    } else
                        CompilerException.throwInvalidArgument(lineCount, 1);
                } else if(line.length > 2)
                    CompilerException.throwInvalidArgumentCount(lineCount, Instruction.MOVE, 2);
            } else
                CompilerException.throwInvalidArgument(lineCount, 0);
        } else
            CompilerException.throwNotEnoughArguments(lineCount, Instruction.MOVE, 1);
    }

    private void handleTURN(String[] line) throws CompilerException {
        if(line.length > 0){
            Instruction direction = Instruction.getByString(line[0]);
            if(direction.getType() == Instruction.IT.Direction){
                writeStm(direction);
            } else
                CompilerException.throwInvalidArgument(lineCount, 1);
        } else
            CompilerException.throwInvalidArgumentCount(lineCount, Instruction.TURN, 1);
    }

    private void handleMINE(String[] line) throws CompilerException {
        if(line.length > 0){
            Instruction direction = Instruction.getByString(line[0]);
            if(direction.getType() == Instruction.IT.Direction){
                writeStm(direction);
            } else
                CompilerException.throwInvalidArgument(lineCount, 1);
        } else
            writeStm(Instruction.FORWARD);
    }

    private void handleECVAVATE(String[] line) {

    }

    private void handleMSG_OWNER(String[] line) throws CompilerException {
        if(line.length > 0){
            write(line, 0);
        } else
            CompilerException.throwInvalidArgumentCount(lineCount, Instruction.MESSAGE_OWNER, 1);
    }

//-----------------------------------------------

    private void writeEndl(){
        byte code = Instruction.LINE_TERMINATOR.getByteCodeValue();
        stream.write(code);
        stream.write(code);
        stream.write(code);
    }

    private void writeStm(Instruction instruction){
        stream.write(instruction.getByteCodeValue());
    }

    private void write(String str){
        for(byte b : str.getBytes()){
            stream.write(b);
        }
    }

    private void write(String[] arr, int startIndex){
        for(int i = startIndex; i < arr.length; i++){
            write(arr[i] + (i != arr.length-1 ? " " : "")); //add spaces to the string
        }
    }

    private void writeBool(boolean b){
        //write(String.valueOf(b));
        stream.write(b ? 0x01 : 0x00);
    }

    private void writeInt(int i) throws IOException{
        stream.write(ByteBuffer.allocate(Integer.SIZE / Byte.SIZE).putInt(i).array());
    }

    private void writeLong(long l) throws IOException{
        stream.write(ByteBuffer.allocate(Long.SIZE / Byte.SIZE).putLong(l).array());
    }

    private void writeFloat(float f) throws IOException {
        stream.write(ByteBuffer.allocate(Float.SIZE / Byte.SIZE).putFloat(f).array());
    }

    private void writeDouble(double d) throws IOException {
        stream.write(ByteBuffer.allocate(Double.SIZE / Byte.SIZE).putDouble(d).array());
    }

    private void writeDataType(Instruction datatype, String[] line) throws IOException {
        if(datatype == Instruction.INT){
            writeInt(Integer.parseInt(line[1]));
        } else if(datatype == Instruction.LONG){
            writeLong(Long.parseLong(line[1]));
        } else if(datatype == Instruction.FLOAT){
            writeFloat(Float.parseFloat(line[1]));
        } else if(datatype == Instruction.DOUBLE){
            writeDouble(Double.parseDouble(line[1]));
        } else if(datatype == Instruction.BOOLEAN){
            writeBool(Boolean.parseBoolean(line[1]));
        } else {
            write(line, 1);
        }
    }

//-----------------------------------------------

    private void handleIF_Helper(String[] line) throws IOException {
        if(variable.containsKey(line[0])){
            writeStm(Instruction.ISVAR);
            writeStm(variable.get(line[0]));
            write(line[0]);
        } else {
            writeStm(Instruction.NONVAR);
            Instruction datatype = DatatypeFinder.toDataType(line[0]);
            writeStm(datatype);
            writeDataType(datatype, line);
        }
    }

    private void handleWhileIF(String[] line, boolean isWhile) throws IOException, CompilerException {
        if(line.length > 0){
            boolean isNegotiated = Instruction.getByString(line[0]) == Instruction.NOT;
            if(isNegotiated) line = Arrays.copyOfRange(line, 1, line.length);

            if(line.length > 3) {  //var == var {
                handleIF_Helper(line);
                writeEndl();

                Instruction compares = (isNegotiated ? NegativeComparison.negotiate(Instruction.getByString(line[1])) : Instruction.getByString(line[1]));
                if(compares.getType() == Instruction.IT.Comparison){
                    handleIF_Helper(Arrays.copyOfRange(line, 2, line.length));
                    writeEndl();

                    writeStm((isWhile ? Instruction.WHILE_START : Instruction.IF_START));
                    lastBrackets.add((isWhile ? Instruction.WHILE_END : Instruction.IF_END));
                } else
                    CompilerException.throwInvalidSyntax(lineCount, "<var0> comparator <var1>");
            } else
                CompilerException.throwInvalidSyntax(lineCount, "<var0> comparator <var1>");
        } else
            CompilerException.throwInvalidSyntax(lineCount, "<var0> comparator <var1>");
    }

}
