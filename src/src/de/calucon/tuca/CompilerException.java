package de.calucon.tuca;

/**
 * Created by Simon on 02.12.2016.
 */
public class CompilerException extends java.lang.Exception {

    private int line;
    private String error;

    public CompilerException(int line, String error){
        this.line = line;
        this.error = error;
    }

//-----------------------------------------------

    public String toBukkitString(){
        return "l:§2" + line + "§7, e:§4 " + error;
    }

    @Override
    public String toString(){
        return "l:" + line + ", e: " + error;
    }

//-----------------------------------------------

    public static void throwNotAStatement(int line, Instruction instruction) throws CompilerException {
        throw new CompilerException(line, instruction.getStringCodeValue() + " is not a valid statement or variable");
    }

    public static void throwNotEnoughArguments(int line, Instruction instruction, int minArgs) throws CompilerException{
        throw new CompilerException(line, instruction.getStringCodeValue() + " requires at least " + minArgs + " argument(s)");
    }

    public static void throwInvalidArgumentCount(int line, Instruction instruction, int args) throws CompilerException{
        throw new CompilerException(line, instruction.getStringCodeValue() + " requires exactly " + args + " argument(s)");
    }

    public static void throwInvalidArgument(int line, int position) throws  CompilerException{
        throw new CompilerException(line, "Invalid Argument at position " + position);
    }

    public static void throwInvalidSyntax(int line, String correction) throws CompilerException {
        throw new CompilerException(line, "Invalid Syntax! " + correction);
    }

}