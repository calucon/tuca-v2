package de.calucon.tuca;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Simon on 04.12.2016.
 */
public enum Instruction {

    //Internal Stuff
    NULL                (0xff, "", IT.Null),                    //printed 3x
    LINE_TERMINATOR     (0x00, "\0", IT.Null),
    ERROR               (0x01, "ERROR", IT.Statement),          //args: string
    DEFINE_VAR          (0x02, "VAR", IT.Statement),
    VAR_ASG             (0x03, "\0", IT.Null),
    LOAD_VAR_TYPE       (0x04, "\0", IT.Null),
    LOAD_VAR            (0x05, "\0", IT.Null),
    GOTO                (0x06, "GOTO", IT.Statement),           //preload GOTOs?
    DEFINE_GOTO         (0x07, ":", IT.Statement),
    STRING              (0x08, "STRING", IT.DataType),
    BOOLEAN             (0x09, "BOOL", IT.DataType),
    INT                 (0x0a, "INT", IT.DataType),
    LONG                (0x0b, "LONG", IT.DataType),
    FLOAT               (0x0c, "FLOAT", IT.DataType),
    DOUBLE              (0x0d, "DOUBLE", IT.DataType),

    //Logic Queries
    NOT                 (0x10, "NOT", IT.Null),                //bzw IFNOT
    IF                  (0x11, "IF", IT.Logic),
    ELSE                (0x12, "ELSE", IT.Logic),
    WHILE               (0x13, "WHILE", IT.Logic),
    IF_START            (0x14, "{", IT.BracketO),                //  {
    ELSE_START          (0x16, "{", IT.BracketO),                //  {
    WHILE_START         (0x16, "{", IT.BracketO),                //  {
    IF_END              (0x15, "}", IT.BracketC),                //  }
    ELSE_END            (0x15, "}", IT.BracketC),                //  }
    WHILE_END           (0x17, "}", IT.BracketC),                //  }

    //Comparison
    NONVAR              (0x20, "\0", IT.Null),                  //says that the following is not a variable
    ISVAR               (0x21, "\0", IT.Null),
    EQU                 (0x22, "==", IT.Comparison),            //  ==
    NEQ                 (0x23, "!=", IT.Comparison),            //  !=
    LSS                 (0x24, "<", IT.Comparison),             //  <
    LEQ                 (0x25, "<=", IT.Comparison),            //  <=
    GTR                 (0x26, ">", IT.Comparison),             //  >
    GEQ                 (0x27, ">=", IT.Comparison),            //  >=
    ASG                 (0x28, "=", IT.Assign),                 //  assign variable

    //Maths
    ADD                 (0x30, "+=", IT.Math),                  //Addition
    SUB                 (0x31, "-=", IT.Math),                  //Subtraction
    MUL                 (0x32, "*=", IT.Math),                  //Multiplication
    DIV                 (0x33, "/=", IT.Math),                  //Division
    EXP                 (0x34, "^=", IT.Math),                  // var ^ x
    MOD                 (0x35, "%=", IT.Math),                  //Modulo

    //Turtle Actions
    MOVE                (0xa0, "MOVE", IT.Statement),           //args: direction, int
    TURN                (0xa1, "TURN", IT.Statement),           //args: direction
    MINE                (0xa2, "MINE", IT.Statement),           //args: direction
    EXCAVATE            (0xa3, "EXCV", IT.Statement),           //args: int, (int)
    MESSAGE_OWNER       (0xa4, "MSG", IT.Statement),            //args: string

    //Direction
    FORWARD             (0xb0, "FWD", IT.Direction),
    BACKWARD            (0xb1, "BWD", IT.Direction),
    LEFT                (0xb2, "LEFT", IT.Direction),
    RIGHT               (0xb3, "RIGHT", IT.Direction),
    UP                  (0xb4, "UP", IT.Direction),
    DOWN                (0xb5, "DOWN", IT.Direction),

    //Turtle Variables
    INT_STORAGE_ITEMS   (0xc0, "STORAGE", IT.Variable),
    INT_FUEL            (0xc1, "FUEL", IT.Variable),
    INT_LOCATION_X      (0xc2, "LOCX", IT.Variable),
    INT_LOCATION_Y      (0xc3, "LOCY", IT.Variable),
    INT_LOCATION_Z      (0xc4, "LOCZ", IT.Variable),
    FACE                (0xc5, "FACE", IT.Variable),
    STORAGE_FULL        (0xc6, "SFull", IT.Variable);
        //boolean storage full etc?

    /*
    TODO
     */



    private byte byteCode;
    private String stringCode;
    private IT type;
    Instruction(int byteCode, String stringCode, IT type){
        this.byteCode = (byte)byteCode;
        this.stringCode = stringCode;
        this.type = type;
    }

//-----------------------------------------------

    public byte getByteCodeValue(){
        return this.byteCode;
    }

    public String getStringCodeValue(){
        return this.stringCode;
    }

    public IT getType(){
        return this.type;
    }

//-----------------------------------------------

    public static Instruction getByString(String str){
        for(Instruction instruction : Instruction.values()){
            if(instruction.getStringCodeValue().equalsIgnoreCase(str))
                return instruction;
        }
        return Instruction.NULL;
    }

//-----------------------------------------------

    public static Map<Byte, Instruction> toMapByte(){
        Map<Byte, Instruction> map = new HashMap<>();
        for(Instruction instruction : Instruction.values()){
            map.put(instruction.getByteCodeValue(), instruction);
        }
        return map;
    }

    public static Map<String, Instruction> toMapString(){
        Map<String, Instruction> map = new HashMap<>();
        for(Instruction instruction : Instruction.values()){
            map.put(instruction.getStringCodeValue(), instruction);
        }
        return map;
    }

//-----------------------------------------------

    //IT -> InstructionType
    public enum IT{
        Statement,
        Logic,
        Comparison,
        Direction,
        Variable,
        BracketO,
        BracketC,
        Assign,
        DataType,
        Math,
        Null
    }

}
